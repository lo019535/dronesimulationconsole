/**
 * 
 */
package DroneSimulation;
import java.util.Random;
import java.util.ArrayList;
import java.lang.String;
/**
 * @author tomis
 *
 */
public class DroneArena {
	//pre-declares the necessary values
	private int arenaSizeX;
	private int arenaSizeY;
	private ArrayList<Drone> droneList = new ArrayList<Drone>();
	Drone newDrone;
	Drone specificDrone;
	
	//the constructor which when DroneArena is initialised sets the size of the arena
	public DroneArena(int x, int y)
	{
		arenaSizeX = x;
		arenaSizeY = y;
	}
	
	//Getters.
	public ArrayList<Drone> getDroneList() {
		return droneList;
	}	
	public int getX()
	{
		return arenaSizeX;
	}
	public int getY()
	{
		return arenaSizeY;
	}
	
	//setters
	public void setX(int newArenaSizeX) {
		this.arenaSizeX = newArenaSizeX;
	}

	public void setY(int newArenaSizeY) {
		this.arenaSizeY = newArenaSizeY;
	}
	
	//this was to add drones from the save files
	public void addSpecificDrone(int version, int x, int y, String str)
	{
		//initiates the direction.
		Direction dir = null;
		//it then checks what the str is. here it would be north south east or west. Tests left in from when there were checks to determine if the class was running.
		if(str.equals("NORTH"))
		{
			 //System.out.println("test2 " + str);
			 specificDrone = new Drone(x,y,Direction.NORTH);
		}
		if(str.equals("EAST"))
		{
			 //System.out.println("test2 " + str);
			 specificDrone = new Drone(x,y,Direction.EAST);
		}
		if(str.equals("SOUTH"))
		{
		     //System.out.println("test2 " + str);
			 specificDrone = new Drone(x,y,Direction.SOUTH);
		}
		if(str.equals("WEST"))
		{
			 //System.out.println("test2 " + str);
			 specificDrone = new Drone(x,y,Direction.WEST);

		}
		//sets the drone ID then adds it to list.
		specificDrone.setVersion(version);
		droneList.add(specificDrone);
	}
	//This is to create a drone not from a save file
	public void addDrone()
	{	
		int posX,posY;
		//gets a random
		Random randomGenerator;
		randomGenerator = new Random();
		//If the droneList size is less than the arena size
		if (droneList.size() < arenaSizeX * arenaSizeY) {
			//while the drone isnt at the position already taken.
			do {
				//add coords. And dont allow it to be instantiated on rim of arena.
				posX = randomGenerator.nextInt(arenaSizeX);
				posY = randomGenerator.nextInt(arenaSizeY);
				if(posX==0)
				{
					posX+=1;
				}
				if(posY==0)
				{
					posY+=1;
				}
				if(posX==arenaSizeX+1)
				{
					posX=-1;
				}
				if(posY==arenaSizeY+1)
				{
					posY=-1;
				}
			} while (getDroneAt(posX, posY) != null);
			//create the drone with a random direction and add to list.
			newDrone = new Drone(posX,posY,Direction.getRandomDirection());
			droneList.add(newDrone);
			
		}
		//if its full!!
		else
		{
			System.out.println("Arena capacity full!!! NO MORE SPACE!!!");
		}

		//System.out.println(d.toString());
		//this was a quick test to make sure it was outputed.
	}
	//This is the function that checks if there is a drone at said position
	public Drone getDroneAt(int x, int y) 
	{
		Drone drone = null;
		//for each drone in dronelist
		for (Drone d : droneList) {
			//then calls where the drone is.
			if (d.isHere(x, y) == true) {
				drone = d;
			}
		}
		return drone;
	}
	
	//to put the drones on the screen. For each drone in dronelist display it.
	public void showDrones(ConsoleCanvas c)
	{
		for(Drone d : droneList)
		{
			d.displayDrone(c);
		}
	}
	//bool to see if the drone can move there.
	public boolean canMoveHere(int x, int y)
	{
		if(getDroneAt(x, y) != null || x >= arenaSizeX || y >= arenaSizeY || x < 0 || y < 0){
			return false;
		}
		else
		{
			return true;
		}
	}
	//function to move every drone randomly once.
	public void moveAllDrones()
	{
		for(Drone d:droneList)
		{
			d.tryToMove(this);
		}
	}
	//the arena toString function.
	public String toString()
	{
		String res = "The arena size is: " + arenaSizeX + "x" + arenaSizeY + " and";
		for (int i = 0; i < droneList.size(); i++) {
			res += "\n" + droneList.get(i).toString();
		}
	    return res;
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}


}
