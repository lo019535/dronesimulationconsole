package DroneSimulation;

public class ConsoleCanvas {
	private char[][] canvas;
	public int canvasX;
	public int canvasY;

	//the constructor that determines the arena size and creates a border with #
	public ConsoleCanvas(int x, int y)
	{
		canvasX = x;
		canvasY = y;
		canvas = new char[x][y];
		for (int i = 0; i < canvasX; i++) 
		{
			for (int j = 0; j < canvasY; j++) 
			{
				canvas[i][j] = ' ';
				if (i == 0) {
					canvas[i][j] = '#';
				}
				if (j == 0) {
					canvas[i][j] = '#';
				}
				if (j == y - 1) {
					canvas[i][j] = '#';
				}
				if (i == x - 1) {
					canvas[i][j] = '#';
				}

			}
		}

	}
	
	//function to place the drone in its position.
	public void showIt(int x, int y, char d)
	{
		canvas[x][y] = d;
	}
	
	//Canvas to string function. this displays the whole arena.
	public String toString()
	{
		String res = "";
		for(int i = 0; i<canvasX; i++)
		{
		    for(int j = 0; j<canvasY; j++)
		    {
		        res = res + canvas[i][j] + " ";
		    }
		    res = res + "\n";
		}
		
		return res;
	}
	
	public static void main(String[] args) {
		//No longer required as this isnt called as its own script
		// TODO Auto-generated method stub
		//ConsoleCanvas c = new ConsoleCanvas(10, 5);	// create a canvas
		//c.showIt(4,3,'D');								// add a Drone at 4,3
		//System.out.println(c.toString());				// display result
	}

}
